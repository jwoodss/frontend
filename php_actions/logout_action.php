<?php
	require_once("../php_functions/global_functions.php");
	$client = establish_connection();
	$params = get_auth_array();
	$client->logOut($params);	
	session_destroy();
	header( "Location: ../login.html" );
?>