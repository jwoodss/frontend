<?php
$servername = "localhost";
$username = "root";
$password = "";
$db_name = "bvj_management";

	function connect_to_db($svr, $usr, $pw, $db){
		// Create connection
		$conn = new mysqli($svr, $usr, $pw, $db);

		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		
		return $conn;
	}
	
	function get_class_list_internal($conn){
		$sql_statement = "SELECT * FROM classes";
		$result = $conn->query($sql_statement);
		return $result;
	}
	
	function disconnect_from_db($conn){
		$conn->close();
	}
	
	function get_class_list_from_db(){
		$connection = connect_to_db("localhost", "root", "", "bvj_management");
		$result = get_class_list_internal($connection);				
		$rows = array();
		try {
			while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
		} catch(Exception $e) {
			echo "Exception: " . $e->getMessage();
		}
		return $rows;
	}
	
	function testing(){
		$connection = connect_to_db("localhost", "root", "", "bvj_management");
		$result = get_class_list_internal($connection);
		if($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				echo "id: " . $row["id"];
			}
		} else {
			echo "no results";
		}
		disconnect_from_db($connection);
		echo "Finished";
	}
	
	function get_students_from_db($class_id = -1){
		$connection = connect_to_db("localhost", "root", "", "bvj_management");
		$sql_statement = "SELECT * FROM students as s";
		If($class_Id != -1){
			$sql_statement .= " INNER JOIN student_class_assignment as sc on sc.student_id = s.id Where sc.class_id = '".$class_id."'"; 
		}
		//echo $sql_statement;
		$result = $connection->query($sql_statement);
		$rows = array();
		while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}		
		return $rows;
	}
	
	function get_absents_for_student($studentId){
		$connection = connect_to_db("localhost", "root", "", "bvj_management");
		$sql_statement = "SELECT * FROM absents WHERE id = ".$studentId;
		$result = $connection->query($sql_statement);
		$rows = array();
		while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}		
		return $rows;
	}
	
	function get_classUser_for_class_from_db($classId, $role_Id){
		$connection = connect_to_db("localhost", "root", "", "bvj_management");
		$sql_statement = "SELECT * FROM user as u INNER JOIN user_class_assignment as uc ON u.id = uc.user_id";
		$sql_statement .= " WHERE uc.class_id = '".$classId."'";
		If($role_Id != -1){
			$sql_statement .= " AND uc.role_id = '".$role_Id."'";
		}
		$result = $connection->query($sql_statement);
		$rows = array();
		while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}		
		return $rows;
	}
	
	?>