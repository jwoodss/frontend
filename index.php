<?php
require_once("php_functions/global_functions.php");
require_once("php_functions/database_functions.php");

$rows = array();
$rows = get_class_list_from_db();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Schülerinformationssystem</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">
	
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Schülerinformationssystem</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                        <i class="fa fa-group fa-fw"></i> Klassen <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
					<?php
						for($i = 0; $i < count($rows); $i++) {
							$classUser = array();
							$classUser = get_classUser_for_class_from_db($rows[$i]['id'], 1);
							print '
								<li>
									<form name="'.$rows[$i]['id'].'" action="index.php?page=schuelerausklasse" method="post">
										<input type="hidden" name="id" value="'.$rows[$i]['id'].'" />
									</form>
									<a href="#" onclick="javascript:document.'.$rows[$i]['id'].'.submit()">
										<div>
											<i class="fa fa-group fa-fw"></i> '.$rows[$i]['id'].'
											<span class="pull-right text-muted small">'.$classUser[0]['mail'].'</span>
										</div>
									</a>
								</li>
								';
								if($i < count($rows) - 1) {
									print '<li class="divider"></li>';
								}
						}
						?>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php print $_SESSION['id']; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="index.php?page=settings"><i class="fa fa-gear fa-fw"></i> Einstellungen</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="php_actions/logout_action.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
				<li>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                        <i class="fa fa-search fa-fw"></i> Suche <i class="fa fa-caret-down"></i>
                    </a>
					<ul class="dropdown-menu dropdown-user" style="width: 250px; padding: 5px;">
                        <li>
						    <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Scouting for Boys...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </li>
                    </ul>
				</li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </nav>

		<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
<?php
	if(isset($_GET['page']) && !empty($_GET['page']))
	{
		 if (file_exists("contents/". $_GET['page'] . ".tpl")):
				 include("contents/". $_GET['page'] .".tpl");
		 else:
				 include("contents/content.tpl");
		 endif;
		 }
	else
		 include("contents/content.tpl");	
?>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <!--<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
	
</body>

</html>
