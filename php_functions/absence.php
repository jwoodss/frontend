<?php
	// testing until the backend has their stuff ready
	require_once('database_functions.php');
	require_once("../php_functions/include_me.php");	
	
	// returns a list of absenses in the form of an array
	// every element is a TO and FROM after one another.
	function getStudentAbsences($studentid) {
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"studentId" => $studentid
		);
		
		return $client->getStudentAbsents($params);
	}
	
	function modify_student_absence(
	$absence_Id = -1,
	$student_Id,
	$startTime,
	$endTime,
	$medicalCertificate,
	$excused,
	$parentsContacted) {		
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"ID" => $absence_Id,
			"StudentID" => $student_Id,
			"From" => $startTime,
			"To" => $endTime,
			"MedicalCertificate" => $medicalCertificate,
			"Excused" => $excused,
			"ParentsContacted" => $excused
		);
		
		return $client->modifyStudentAbsence($params);
	}
	
	// Delete an absence.
	function delete_absence($absence_Id){
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"ID" => $absence_Id		
		);
		// Delete Absences
		//return $client->DeleteStudentAbsents($params);
	}
?>