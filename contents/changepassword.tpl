﻿<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Einstellungen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
		 <form action="" method="post">
		    
			<label style="font-size:12pt">Passwort ändern</label>
            <div class="row">
                <div class="col-lg-6">
                    Altes Passwort:<input class="form-control" type="password">
                    Neues Passwort:<input class="form-control" type="password">
                    Neues Passwort (nochmal):<input class="form-control" type="password">
                </div>
            </div>
			</label>
				
				<div class="row">
					<div class="col-lg-6">
						<input type="submit" value="Passwort ändern" class="btn btn-lg btn-success btn-block"/>
					</div>
				</div>
				
		 </form>
    </div>
</div>