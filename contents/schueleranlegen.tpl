﻿<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Schüler anlegen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<form name="new_student" method="post" action="index.php?page=schueler">

<!-- /.row -->
<div class="row">
    <div class="col-lg-8">

        <!--<div class="form-group">-->
        <label style="font-size:12pt">Schüler</label>
        <div class="row">
            <div class="col-lg-6">
                <input class="form-control" name="firstname" placeholder="Vorname">
                <input class="form-control" name="lastname" placeholder="Nachname">
                <div class="col-lg-6" style="padding-right: 0; padding-left: 0;">
                    <select class="form-control" name="gender">
                        <option disabled selected>Geschlecht</option>
                        <option value="m">männlich</option>
                        <option value="w">weiblich</option>
                    </select>
                </div>
                <div class="col-lg-6" style="padding-right: 0; padding-left: 0;">

                    <input class="form-control" type="date">

                </div>
                <select class="form-control" name="class">
                    <option disabled selected>Klasse</option>
                </select>

            </div>
        </div>
    </div>
</div>


<label style="margin-top:15px; font-size:12pt">Kontakt</label>

<div class="row">
    <div class="col-lg-4">
	
        <div class="col-lg-4" style="width: 80%; padding-right: 0; padding-left: 0;">
            <input class="form-control" name="street" placeholder="Straße">
        </div>

		<div class="col-lg-4" style="width: 20%; padding-right: 0; padding-left: 0;">
            <input id="Hsnr" class="form-control" name="number" placeholder="Nr.">
        </div>

		<div class="col-lg-4" style="width:25%; padding-right: 0; padding-left: 0;">
            <input class="form-control" name="zip" placeholder="Postleitzahl">
        </div>
		
		<div class="col-lg-2" style="width:40%; padding-right: 0; padding-left: 0;">
			<input class="form-control" name="town" placeholder="Wohnort">
        </div>
	
	</div>
</div>

</form>