﻿<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Fehlzeit eintragen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-8">

        <form action="" method="post">
            <label style="font-size:12pt">Schüler</label>
            <div class="row">
                <div class="col-lg-6">
                    <input class="form-control" placeholder="Vorname" readonly>
                    <input class="form-control" placeholder="Nachname" readonly>
                    <input class="form-control" placeholder="Klasse" readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="col-lg-6" style="padding:0px 5px 0px 0px;">
                        <label>Startdatum</label>
                        <input class="form-control" type="date">
                    </div>
                    <div class="col-lg-6" style="padding:0px 0px 0px 5px;">
                        <label>Uhrzeit</label>
                        <input class="form-control" type="time">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="col-lg-6" style="padding:0px 5px 0px 0px;">
                        <label> Enddatum</label>
                        <input class="form-control" type="date">
                    </div>
                    <div class="col-lg-6" style="padding:0px 0px 0px 5px;">
                        <label>Uhrzeit</label>
                        <input class="form-control" type="time">
                    </div>
					<input type="submit" value="anlegen" class="btn btn-lg btn-success btn-block"/>
                </div>
            </div>
    </div>
    </form>
</div>