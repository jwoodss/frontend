<?php
	require_once("../php_functions/global_functions.php");
	
	function insert_user($firstName, $lastName, $userName, $password) {
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"firstName" => $firstName,
			"lastName" => $lastName,
			"userName" => $userName,
			"password" => $password			
		);
		
		return $client->addUser($params);
	}
	
	function remove_user($userid) {
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"userId" => $userid		
		);
		
		return $client->removeUser($params);
	}
	
	/*function change_user_password() {
		
	}*/
?>