<?php
	require_once("../php_functions/global_functions.php");
	
	function get_class_list() {
		$params = array();	
		$client = establish_connection();
		$classes = $client->getClasses($params)->return;
		$return = array();
		foreach($classes->key as $key=>$value ){
			$dataset = array($value, $classes->value[$key]);
			array_push($return, $dataset);
		}
		//return $return;
		return get_class_list_from_db();
	}
	
	function get_classUser_for_class($class_Id, $role_Id = -1){
		return get_classUser_for_class_from_db($class_Id, $role_Id);
	}
?>