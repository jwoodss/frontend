<?php	
	require_once( 'secure_functions.php' );

	function establish_connection(){
		return new SoapClient("http://10.161.8.81:8080/SoapInterface/services/SoapConnectionManager?wsdl");
	}
	
	function get_auth_array(){
		if(isset($_SESSION['auth'])){
			$auth_array = array(
				"authMD5" => $_SESSION['auth']
			);
			return $auth_array;
		}else{
			header("Location: ../php_actions/logout_action.php");
		}
	}
	
	function get_content($page){
		check_rights( $page );
		switch( $page ){
			case 'index':
				require_once('index.php');
				break;
			default: 
				header( 'Location: error.php' );
				break;
		}		
	}
	
?>