﻿<?php
$rows = array();
$rows = get_class_list_from_db();

$colors = array("primary", "green", "yellow", "red");

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Klassen</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	
		<?php
			for($i = 0; $i < count($rows); $i++) {
			$classUser = array();
			$classUser = get_classUser_for_class_from_db($rows[$i]['id'], 1);
			print '	
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-'.$colors[$i % 4].'">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">'.$rows[$i]['id'].'</div>
						<div>'.$classUser[0]['mail'].'</div>
					</div>
				</div>
			</div>
			<form name="form'.$rows[$i]['id'].'" action="index.php?page=schuelerausklasse" method="post">
			<input type="hidden" name="id" value="'.$rows[$i]['id'].'" />
			<a href="#" onclick="javascript:document.form'.$rows[$i]['id'].'.submit();">
				<div class="panel-footer">
					<span class="pull-left">Klasse anzeigen</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
			</form>
		</div>
	</div>		
			';	
			}
		?>
	<!--
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">11FI1</div>
						<div>Wallner</div>
					</div>
				</div>
			</div>
			<a href="index.php?page=schuelerausklasse">
				<div class="panel-footer">
					<span class="pull-left">Klasse anzeigen</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-green">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">11FI2</div>
						<div>Lange</div>
					</div>
				</div>
			</div>
			<a href="index.php?page=schuelerausklasse">
				<div class="panel-footer">
					<span class="pull-left">Klasse anzeigen</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-yellow">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">11FI3</div>
						<div>Zobel</div>
					</div>
				</div>
			</div>
			<a href="index.php?page=schuelerausklasse">
				<div class="panel-footer">
					<span class="pull-left">Klasse anzeigen</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-red">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">11FI4</div>
						<div>Steinam</div>
					</div>
				</div>
			</div>
			<a href="index.php?page=schuelerausklasse">
				<div class="panel-footer">
					<span class="pull-left">Klasse anzeigen</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	-->
</div>
<!-- /.row -->