<?php
	require_once("../php_functions/global_functions.php");
	
	// get_student_list
	// get_student_name
	// add_student
	// remove_student
	// get_student_absences
	
	function get_student_list($class_Id = -1) {		
		return get_students_from_db($class_Id);
	}
	
	function get_student_name($student_Id) {	
		$client = establish_connection();
		$auth = get_auth_array();
		$params = array(
			"authMD5" => $auth["authMD5"],
			"studentID" => $student_Id);
		return $client->getStudentName($params);
	}
	
	// if a 2 comes back from the function, then it's an insertion error
	function add_student(
		$firstname,
		$lastname,
		$birthdate,
		$classid,
		$email,
		$phone,
		$address,
		$gender
		)
	{
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"firstName" => $firstname,
			"lastName" => $lastname,
			"birthdate" => $birthdate,
			"classId" => $classid,
			"email" => $email,
			"phoneNumber" => $phone,
			"address" => $address,
			"gender" => $gender
		);
		return $client->addStudent($params);
	}
	
	// returns a list of absenses in the form of an array
	// every element is a TO and FROM after one another.
	function get_student_absences($studentid) {
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"studentId" => $studentid
		);
		
		return $client->getStudentAbsents($params);
	}
	
	function get_student_details($studentid){
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"studentId" => $studentid
		);
		
		return $client->getStudentDetails($params);
	}
	
	function remove_student($studentid) {
		$client = establish_connection();
		$auth = get_auth_array();
		
		$params = array(
			"authMD5" => $auth["authMD5"],
			"studentId" => $studentid
		);
		
		return $client->deleteStudent($params);
	}
	
?>