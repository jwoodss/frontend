﻿<?php
$id   = $_POST['id'];
$rows = array();
$rows = get_students_from_db($id);

?>

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Schüler der Klasse
					<?php
						print $_POST['id'];
					?>
</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						    <!-- /.table-responsive -->
                            <div class="well">
                                <!--<h4>DataTables Usage Information</h4>
                                <p>Dis is for your informaischen.</p>-->
                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="index.php?page=schueleranlegen">Neuen Schüler anlegen</a>
                            </div>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Vorname</th>
                                            <th>Nachname</th>
                                            <th>Fehlzeiten</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									for($i = 0; $i < count($rows); $i++) {
										$classUser = array();
										$classUser = get_classUser_for_class_from_db($id, 1);
										print '
											<tr class="odd gradeX">
												<td><a href="index.php?page=schuelerdetails">'.utf8_encode($rows[$i]['firstname']).'</a></td>
												<td><a href="index.php?page=schuelerdetails">'.utf8_encode($rows[$i]['lastname']).'</a></td>
												<td class="center">';
												
												$absents = array();
												$absents = get_absents_for_student($rows[$i]['id']);
												$time  = 0;
												for ($j = 0; $j < count($absents); $j++)
												{
													$time += $absents['to'] - $absents['from'];
												}
												print $time . " ID: " . $rows[$i]['id'];
												print '</td>
											</tr>
										';
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
		<!-- /.row -->