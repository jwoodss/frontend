DROP DATABASE IF EXISTS bvj_management;

CREATE DATABASE bvj_management;

USE bvj_management;

CREATE TABLE `user`(
	`id` varchar(32) not null,
	`mail` varchar(32) not null unique,
	`pw` varchar(32) not null,
	primary key( `id` )
)engine=innoDB;

CREATE TABLE `classes`(
	`id` varchar(32) not null,
	`name` varchar(32) not null,
	primary key( `id` )
)engine=innoDB;


CREATE TABLE `roles`(
	`id` int(32) auto_increment not null,
	`name` varchar(32) not null,
	`read` boolean,
	`write` boolean,
	`edit` boolean,
	primary key( `id` )
)engine=innoDB;

CREATE TABLE `user_class_assignment`(
	`user_id` varchar(32) not null,
	`class_id` varchar(32) not null,
	`role_id` int(32) not null,
	primary key( `user_id`, `class_id`, `role_id` ),
	foreign key( `user_id` ) references `user`(`id`),
	foreign key( `class_id` ) references `classes`(`id`),
	foreign key( `role_id` ) references `roles`(`id`)
)engine=innoDB;

CREATE TABLE `students`(
	`id` int(32) auto_increment not null,
	`firstname` varchar(32) not null,
	`lastname` varchar(32) not null,
	`birthdate` varchar(32) not null,
	primary key( `id` )
)engine=innoDB;

CREATE TABLE `student_class_assignment`(
	`student_id` int(32) not null,
	`class_id` varchar(32) not null,
	primary key( `student_id`, `class_id` ),
	foreign key( `student_id` ) references `students`(`id`),
	foreign key( `class_id` ) references `classes`(`id`)
)engine=innoDB;

CREATE TABLE `absents`(
	`id` int(32) auto_increment not null,
	`student_id` int(32) not null,
	`from` timestamp,
	`to` timestamp,
	`excused` boolean,
	`attest` boolean,
	`parents_contact` boolean,
	primary key( `id` ),
	foreign key( `student_id` ) references `students`(`id`)
)engine=innoDB;

##############################################TESTDATEN############################################################

INSERT INTO `user` VALUES ( 'user1', 'mail1@mail.de', 'pw1' );
INSERT INTO `user` VALUES ( 'user2', 'mail2@mail.de', 'pw2' );

INSERT INTO `classes` VALUES ( 'BVJ2015', 'BVJ' );
INSERT INTO `classes` VALUES ( 'BVJ2014', 'BVJ' );
INSERT INTO `classes` VALUES ( 'BVJ2013', 'BVJ' );

INSERT INTO `roles` VALUES ( 0, 'Klassenleiter', true, true, true );
INSERT INTO `roles` VALUES ( 0, 'Lehrer', true, false, false );
INSERT INTO `roles` VALUES ( 0, 'Extern', true, true, false );

INSERT INTO `user_class_assignment` VALUES ( 'user1', 'BVJ2013', 3 );
INSERT INTO `user_class_assignment` VALUES ( 'user2', 'BVJ2013', 1 );
INSERT INTO `user_class_assignment` VALUES ( 'user1', 'BVJ2014', 1 );
INSERT INTO `user_class_assignment` VALUES ( 'user2', 'BVJ2014', 2 );
INSERT INTO `user_class_assignment` VALUES ( 'user1', 'BVJ2015', 3 );
INSERT INTO `user_class_assignment` VALUES ( 'user2', 'BVJ2015', 2 );

INSERT INTO `students` VALUES ( 0, 'Max', 'Mustermann', '01.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Anton', 'Affe', '02.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Bernd', 'Brot', '03.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Charlie', 'Cello', '04.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Dora', 'Dussel', '05.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Emil', 'Elend', '06.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Frank', 'Funkel', '07.01.1990' );
INSERT INTO `students` VALUES ( 0, 'Gustav', 'Grün', '08.01.1990' );

INSERT INTO `student_class_assignment` VALUES ( 1, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 2, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 3, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 4, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 5, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 2, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 3, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 4, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 1, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 6, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 5, 'BVJ2014' );
INSERT INTO `student_class_assignment` VALUES ( 1, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 2, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 3, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 4, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 5, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 6, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 7, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 8, 'BVJ2015' );
INSERT INTO `student_class_assignment` VALUES ( 6, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 7, 'BVJ2013' );
INSERT INTO `student_class_assignment` VALUES ( 8, 'BVJ2013' );

INSERT INTO `absents` VALUES ( 0, 1, '2015-02-03 08:00:00', '2015-02-03 09:00:00', 0, 0, 0 );
INSERT INTO `absents` VALUES ( 0, 1, '2015-02-03 08:00:00', '2015-02-03 09:00:00', 0, 0, 0 );
INSERT INTO `absents` VALUES ( 0, 1, '2015-02-03 08:00:00', '2015-02-03 09:00:00', 0, 0, 0 );


